#!/usr/bin/env python3.5

n = int(input())
m = int(input())
for i in range(n,m+1):
    if i%15 == 0:
        print("FizzBuzz")
    elif i%3 == 0:
        print("Fizz")
    elif i%5 == 0:
        print("Buzz")
    else:
        print(i)